import 'package:toktik/domain/entities/video_post.dart';

abstract class VideoPostDataSource {
  Future<List<VideoPost>> getFavoriteVideoPostUser(String userID);
  Future<List<VideoPost>> getTrendingVideosPage(int page);
}
