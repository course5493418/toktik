import 'package:toktik/domain/datasources/video_posts_datasource.dart';
import 'package:toktik/domain/entities/video_post.dart';
import 'package:toktik/domain/repositories/video_posts_datasource.dart';

class VideoPostsRepositoryIpml implements VideoPostRepository {
  final VideoPostDataSource videosDataSource;

  VideoPostsRepositoryIpml({required this.videosDataSource});
  @override
  Future<List<VideoPost>> getFavoriteVideoPostUser(String userID) {
    throw UnimplementedError();
  }

  @override
  Future<List<VideoPost>> getTrendingVideosPage(int page) {
    return videosDataSource.getTrendingVideosPage(page);
  }
}
